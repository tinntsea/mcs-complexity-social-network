package iei.complexity;

import iei.complexity.user.Graph;
import iei.complexity.user.Vertex;
import org.jgrapht.alg.clique.BronKerboschCliqueFinder;
import org.jgrapht.alg.clustering.GirvanNewmanClustering;
import org.jgrapht.alg.clustering.KSpanningTreeClustering;
import org.jgrapht.alg.clustering.LabelPropagationClustering;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.interfaces.ClusteringAlgorithm;
import org.jgrapht.alg.interfaces.StrongConnectivityAlgorithm;
import org.jgrapht.alg.scoring.ClusteringCoefficient;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultUndirectedGraph;

import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws Exception {
        int numCommunities;

        // 1. Generate a random social network, represented as a graph.
        // create random graph with 50k user, each user has 1k friends
//        numCommunities = CountCommunity(Graph.genRandomNetwork(2, 1));
//        System.out.println("Number of communities in the network: " + numCommunities);

        // 2. Generate a network with N communities, each has M members
        // create network with 5000 community, each has 100 users (500k users total)
        int numCommInput = 5;
        int numUsersInput = 10;

        Graph network = Graph.genNetwork(numCommInput, numUsersInput);

        CountCommunityCliques(network, numUsersInput);

        numCommunities = CountCommunityGirvanNewman(network, numCommInput);
//        System.out.println("Number of communities in the network: " + numCommunities);

        numCommunities = CountCommunityLabelPropagationClustering(network, 20);
//        System.out.println("Number of communities in the network: " + numCommunities);
    }

    public static int CountCommunityCliques(Graph network, int commSize) {
        System.out.println("\n>>> Community Detection using Clique Counting");
        int nodesInGraphs = network.edges.size();
        for (int i = 0; i < nodesInGraphs; i++) {
            for (int j = 0; j < network.edges.get(i).size(); j++) {
                Clique.graph[network.edges.get(i).get(j).from().id()][network.edges.get(i).get(j).to().id()] = 1;
                Clique.graph[network.edges.get(i).get(j).to().id()][network.edges.get(i).get(j).from().id()] = 1;
                Clique.d[network.edges.get(i).get(j).from().id()]++;
                Clique.d[network.edges.get(i).get(j).to().id()]++;
            }
        }

        Clique.n = network.vertices.size();

        long startTime = System.currentTimeMillis();

        Clique.findCliques(0, 1, commSize);

        long endTime = System.currentTimeMillis();

        System.out.println("\nIt took " + (endTime - startTime) + " ms");

        return 0;
    }

    public static int CountCommunityGirvanNewman(Graph network, int numComm) {
        System.out.println("\n>>> Community Detection using Girvan-Newman Algorithm");
        DefaultDirectedGraph<String, DefaultEdge> graph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
//        DefaultUndirectedGraph<String, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);

        for (Vertex v : network.vertices) {
            graph.addVertex(String.valueOf(v.id()));
        }

        int nodesInGraphs = network.edges.size();
        for (int i = 0; i < nodesInGraphs; i++) {
            for (int j = 0; j < network.edges.get(i).size(); j++) {
                graph.addEdge(
                    String.valueOf(network.edges.get(i).get(j).from().id()),
                    String.valueOf(network.edges.get(i).get(j).to().id())
                );
            }
        }

        long startTime = System.currentTimeMillis();

        // computes all the strongly connected components of the directed graph
        ClusteringAlgorithm<String> cAlg = new GirvanNewmanClustering<>(graph, numComm);
        List<Set<String>> clusters = cAlg.getClustering().getClusters();

        long endTime = System.currentTimeMillis();

        System.out.println("\nIt took " + (endTime - startTime) + " ms");

        int i = 1;
        for (Set<String> c : clusters) {
            System.out.print("\nCommunity " + i++ + ": ");
            for (String n : c) {
                System.out.print(n + " ");
            }
        }
        System.out.println("");

        return cAlg.getClustering().getNumberClusters();
    }

    public static int CountCommunityLabelPropagationClustering(Graph network, int numComm) {
        System.out.println("Community Detection using Label Propagation Clustering Algorithm");
        DefaultUndirectedGraph<String, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);

        for (Vertex v : network.vertices) {
            graph.addVertex(String.valueOf(v.id()));
        }

        int nodesInGraphs = network.edges.size();
        for (int i = 0; i < nodesInGraphs; i++) {
            for (int j = 0; j < network.edges.get(i).size(); j++) {
                graph.addEdge(
                    String.valueOf(network.edges.get(i).get(j).from().id()),
                    String.valueOf(network.edges.get(i).get(j).to().id())
                );
            }
        }

        long startTime = System.currentTimeMillis();

        // computes all the strongly connected components of the directed graph
        LabelPropagationClustering<String, DefaultEdge> cAlg = new LabelPropagationClustering<>(graph, numComm);
        List<Set<String>> clusters = cAlg.getClustering().getClusters();

        long endTime = System.currentTimeMillis();

        System.out.println("\nIt took " + (endTime - startTime) + " ms");

        int i = 1;
        for (Set<String> c : clusters) {
            System.out.print("\nCommunity " + i++ + ": ");
            for (String n : c) {
                System.out.print(n + " ");
            }
        }
        System.out.println("");

        return cAlg.getClustering().getNumberClusters();
    }
}
