package iei.complexity.user;

public class Edge {
    private Vertex from;
    private Vertex to;
    private float weight;

    public Edge(Vertex from, Vertex to, float weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public Vertex from() {
        return from;
    }

    public Vertex to() {
        return to;
    }

    public float weight() {
        return weight;
    }
}
